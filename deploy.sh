#Install Ruby
apt install -y ruby-dev

# Install dpl for heroku
gem install dpl

# S'occupe de l'envoi automatic à Heroku
dpl --provider=heroku --app=ci-20-4 --api-key=$HEROKU_STAGING_API_KEY